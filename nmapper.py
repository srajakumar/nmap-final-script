from lxml import etree
import subprocess
import csv
import os
import pdb
from os.path import isfile,join
from os import listdir
import shutil
import sys
import argparse
import time
import openpyxl
from multiprocessing import Process
import time
import math
import re
from rancidcmd import RancidCmd



INPUT_ERROR = "File must be aow being the headers and the data beginning in row 2.  There must be one column with header MAC_Address and one with header IP_Address.  The output of the switch_info script works perfectly."

# run a Unix command and return a variable with the text output from stdout
def run_cmd(cmd):
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    return proc.stdout.read()
    
# convert a single file from csv to xlsx format to prevent text conversion
def convert_csv(input_filename, output_filename):
    file = open(input_filename)
    workbook = openpyxl.Workbook()
    worksheet = workbook.active
    reader = csv.reader(file, delimiter=',', quotechar='"')
    
    # pull every line in the csv file
    for line in reader:      
        # copy each line to the new file
        worksheet.append(line)
    file.close()
    
    # save the output .xlsx file
    workbook.save(output_filename)
    #csv file with the first r
    return 
    
    
class ConfigFile:
    __paramlist=[]
    def __init__(self, input_file, write_not_profiled=False, dbfile=None, switch_file):
        self.input_file = input_file
        self.switch_list = switch_file
        self.paramlist = []
        self.csvheaders = []
        self.profiledlist=[]
        self.MACLst=[]
        self.ipLst=[]
        self.hostLst=[]
        self.write_not_profiled=write_not_profiled
        self.dbfile = dbfile
        self.output_file = input_file.replace(".csv", "-out123.csv") #file extension to be added later
           
    def parseCsvFile(self):
        try:
            with open(self.input_file) as csvfile:
                linereader = csv.reader(csvfile, delimiter=',', quotechar='"')
                line = 0
                for row in linereader:
                    if len(row)>1 and line == 0:
                        self.csvheaders = row[:]
                        if "IP_Address" not in self.csvheaders or "MAC_Address" not in self.csvheaders:
                            print str(self.csvheaders)
                            sys.exit(INPUT_ERROR + "\n" + "Error in headers")
                    elif len(row) == len(self.csvheaders):
                        line_parameters = {}
                        column = 0
                        for item in row:
                            line_parameters[self.csvheaders[column]] = item
                            column += 1
                        self.paramlist.append(line_parameters)
                    else:
                        sys.exit(INPUT_ERROR + "\n" + "Error reading row: %s" % str(row))
                    line += 1
        except csv.Error as e:
            sys.exit('file {}, line {}: {}'.format(csvconfigfile, linereader.line_num, e))
        print "Number of IPs to process = " + str(len(self.paramlist))
    

    def runParallelParseDeviceType(self, splits):
        print "Running up to %d nmap threads in parallel" % splits
        proc = []
        total_lines = len(self.paramlist)
        for i in xrange(splits):
            start_line = i*math.ceil(float(total_lines) / splits)
            end_line = (i + 1)*math.ceil(float(total_lines) / splits) - 1
            if end_line >= total_lines:
                end_line = total_lines -1
            if start_line >= total_lines:
                break
            print "Starting thread %d for rows %d through %d" % (i, start_line, end_line)
            p = Process(target=self.parseDeviceType, args=(True, start_line, end_line))
            p.start()
            proc.append(p)
            
        print "Waiting for all threads to finish"
        for p in proc:
            p.join()
        
        print "All threads have finished"
            
    
    def parseDeviceType(self, parallel=False, start_line=None, end_line=None):
        line = 0
        for row in self.paramlist:
            if not parallel or (parallel and line >= start_line and line <= end_line):            
                ip = row['IP_Address']
                print "Running nmap for %s" % ip
                
                out = "Not in use"
                if "no" in ip.lower():
                    print "Skipped nmap for %s" % ip
                else:
                    out = run_cmd("nmap -A -Pn -T4 -oX - %s" % ip)
                    print "Completed nmap for %s" % ip

                path = 'dbresult'

                if not os.path.exists(path):
                    os.makedirs(path)

                filename = str(ip)
                with open(os.path.join(path, filename), 'wb') as temp_file:
                    temp_file.write(out)
            
            line += 1

    def parseMacAddress(self):
        for row in self.paramlist[1:]:
            mac = row['MAC_Address']
            splitedmac  = mac.split(':')
            joinedmac = ':'.join(a.zfill(2) for a in splitedmac )
            row['MAC_Address']=joinedmac
                
    def parseXmlResults(self):
        dbfile = "dbresult"
        
        if self.dbfile:
            dbfile = self.dbfile

        allfiles = [f for f in listdir(dbfile) if isfile(join(dbfile,f))]
        print ("allfileslength = "+str(len(allfiles)))
                       
        for fil in allfiles:
            with open(dbfile + "/" + fil, 'r') as myfile:
                out=myfile.read().replace('\n', '')
            try:
                re = self.findsomething(out)
            except:
                re = None
            path = None
            if re == None:
                path = 'notprofiled'
                if self.write_not_profiled:
                    if not os.path.exists(path):
                        os.makedirs(path)
                    with open(os.path.join(path, fil), 'wb') as temp_file:
                        temp_file.write(out)
            else:
                path = 'profiled'
            self.updateCsvRecord(fil,re)

        self.writefinalcsv()
    
    def writefinalcsv(self):
        with open(self.output_file, 'wb') as temp_file:
            headers = "\",\"".join(self.csvheaders)
            headers = "\"" + headers + "\""
            headers += ",\"Classification/Device_Type (Prediction from IP)\",\"OUI (Prediction from MAC)\""
            temp_file.write(headers+'\n')
            for row in self.profiledlist:
                values = []
                for header in self.csvheaders:
                    values.append(row[header])
                values.append(row['Classification/Device_Type'])
                values.append(row['OUI'])
                temp_file.write(",".join(values) + '\n')
            
                    
    def updateCsvRecord(self,fil,ret):
        for row in self.paramlist:
            ip = row['IP_Address']
            MAC = row['MAC_Address']
            if ip == fil:
                
                print(ip)
                print(MAC)
                predicted_OUI = run_cmd("curl -s https://api.macvendors.com/%s" % MAC)
                time.sleep(2)
                if "error" in predicted_OUI:
                    predicted_OUI = "Unknown"
                    print("Unknown")
                profiled_dict = row.copy()
                profiled_dict['OUI'] = predicted_OUI

                if re.search(r'Apple', predicted_OUI):
                    print("Apple matched")
                    ret = 'Mac OS'
                if re.search(r'Windows', predicted_OUI):
                    print("Windows matched")
                    ret = 'Windows Machine'
                                    
                if ret:
                    profiled_dict['Classification/Device_Type']= ret
                else:
                    profiled_dict['Classification/Device_Type']= "Unknown"
                
                
                self.profiledlist.append(profiled_dict)
                ret = "Unknown"
                
            
    def findsomething(self, xmll):
        tree = etree.fromstring(xmll)
        voip_protocols = ['cisco-sccp','sip']
        extrainfos = ['fedora','centos']

        
        
        count =0
        service = (tree.findall(".//service"))
        osmatch = (tree.findall(".//osmatch"))
        devicetype = None
        
                            
        
        for svc in service:             
            if 'devicetype' in svc.keys():
                devicetype = svc.get('devicetype')
                print ("2.DEVICETYPE:"+devicetype)
                return devicetype

        max_os_accuracy = 0
        osmatchChecked=0
        
        for osm in osmatch:
            accuracy = osm.get('name')
            print(accuracy)
            if re.search(r'Linux', accuracy):
                print("Accuracy matched")
                devicetype = accuracy
                osmatchChecked=1;
                print ("5.DEVICETYPE:"+ devicetype)
            elif re.search(r'Mac', accuracy):
                print("Accuracy matched")
                devicetype = accuracy
                osmatchChecked=1;
                print ("5.DEVICETYPE:"+ devicetype)
            elif re.search(r'Windows', accuracy):
                print("Accuracy matched")
                devicetype = accuracy
                osmatchChecked=1;
                print ("5.DEVICETYPE:"+ devicetype)
                
            
                    
                    
                    
        if osmatchChecked ==1:
            return devicetype
            
            #return devicetype
            
        else:
            for svc in service:             
                if (svc.get('name')) in voip_protocols:
                    print ("4.DEVICETYPE: Phone")
                    devicetype = 'Phone'
                    return devicetype
        
            for svc in service:
                if 'extrainfo' in svc.keys():
                    extrainfo = svc.get('extrainfo')
                    lin = (extrainfo.lower().split())
                    for ein in extrainfos:
                        for j in lin:
                            if ein in j:
                                devicetype = ein
                                print ("4.DEVICETYPE:"+ein)
                                return devicetype
    def getMacPort(self):
        '''
        Using RancidCmd we login to the list of switch and execute the show mac port command 
        and store the MAC address and Port details in CSV file.
        '''
        
        print self.switch_list
        fh= open(self.input_file,'a')
        writer = csv.writer(fh,dialect='excel')
        writer.writerow(["MAC_Address " "VLAN " "PORT "])

        with open(self.switch_list,'r') as swlst:
                for item in swlst:
                        cmdLst = ["show mac port ge.*.* type learned", "show mac port tg.*.* type learned"]
                        for cmd in cmdLst:
                                rancid = RancidCmd(login='/usr/libexec/rancid/clogin',address=item)
                                csfile= rancid.execute(cmd)
                                if csfile['rtn_code'] !=0:
                                    print "Command failed with code {0} and error message {1}".format(csfile['rtn_code'],csfile['std_err'])
                                    sys.exit()
                                for row in csfile['std_out'].splitlines():
                                    if re.search(r'^([0-9a-fA-F]{1,2}[-][0-9a-fA-F]{1,2}[-][0-9a-fA-F]{1,2}[-][0-9a-fA-F]{1,2}[-][0-9a-fA-F]{1,2}[-][0-9a-fA-F]{1,2})',row):
                                        nrow=re.sub(" +"," ",row)
                                        lstrow=nrow.split(" ")
                                        writer.writerow([" ".join(lstrow[:-2])])
        fh.close()
        #return self.input_file
    
    def formatMAC(self):
        file = open(self.input_file,'r')
        next(file)
        for line in file:
                currentMAC = line.split(" ")
                updatedMACLst=currentMAC[0].split('-')
                self.MACLst.append(updatedMACLst[0]+updatedMACLst[1]+"."+updatedMACLst[2]+updatedMACLst[3]+"."+updatedMACLst[4]+updatedMACLst[5])
        
        #return self.MACLst
    
    def getIPList(self):
        
        ipAd=""
        
        for ad in self.MACLst:
                cmd = "sh arp " + str(ad).lower().strip()
                rnid = RancidCmd(login='/usr/libexec/rancid/clogin',address="rtr-distro-cal")
                ipAd = rnid.execute(cmd)
                if ipAd['rtn_code'] !=0:
                        print "Command failed with code {0} and error message {1}".format(ipAd['rtn_code'],ipAd['std_err'])
                        sys.exit()

                for row in ipAd['std_out'].splitlines():
                        
                        if re.search(r'^([0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3})',row):
                            word = row.split(' ')
                            self.ipLst.append(word[0].strip())
                        else:
                            self.ipLst.append("None")
        updateCSVFile(self.ipLst,self.input_file,"IP_Address")
        #return self.ipLst
    
    def getHostname(self):
        
        for ip in ipLst:
            ip=ip.strip()
            if ip!= "None":
                proc = run_cmd("nslookup "+ip)
                for line in proc:
                    if "name = " in line:
                        host=line.split('name =')
                        self.hostLst.append(host[0].strip)
            else:
                self.hostLst.append("No Hostname")
        updateCSVFile(self.hostLst,self.input_file,"Hostname")
        #return self.hostLst
    
    def updateCSVFile(self,lstData,header):
        f = open(self.input_file)
        data = [item for item in csv.reader(f)]
        f.close()
        lstData[0] = header
        new_column = lstData

        new_data = []

        for i, item in enumerate(data):
                item.append(new_column[i])
                new_data.append(item)
                f = open(self.input_file, 'w')
                csv.writer(f,delimiter=' ').writerows(new_data)
                f.close()
    
                
                
def main(args):
    
    parser = argparse.ArgumentParser(description="Find device types from devices in cut sheet")
    parser.add_argument('-p', help="Process the IPs from an already created DB folder", action="store_true")
    parser.add_argument('-n', help="Only run nmap to create the DB folder", action="store_true")
    parser.add_argument('-w', help="Add this flag to output devices that haven't been profiled successfully to a folder", action="store_true")
    parser.add_argument('-c', help="Store final result in CSV instead of xlsx format", action="store_true")
    parser.add_argument('--dbfile', help="Specify the DB folder for input, only when using -p option")
    parser.add_argument('--parallel', help="Specify the number of parallel threads that should be used.  If not specified, only 1 thread will be used.",  type=int)
    parser.add_argument('path', help="Path to CSV to process")
    parser.add_argument('switch', help="List of Switch entries")
    
    
    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))
        return
    
    
        
    write_not_profiled = False
    if args.w:
        write_not_profiled = True
        
    csvobj = ConfigFile(args.path, write_not_profiled, args.dbfile, args.switch)
    
    #Login to list of switch and execute the show mac port command and obtain the MAC address which is then stored in a CSV file.
    print "Log into the list of switch and get the MAC address and store it in CSV"
    csvobj.getMacPort()
    

    print "Format the MAC address to the form XXXX.XXXX.XXXX such that it is used to obtain IP using sh arp"
    csvobj.formatMAC()
    
    print "Format the MAC address to the form XXXX.XXXX.XXXX such that it is used to obtain IP using sh arp"
    csvobj.getIPList()
    
    
    
    
    #Checking for the existence for CSV file after creating it .
    if ".csv" not in args.path:
        sys.exit(INPUT_ERROR)
        
    print "Parsing the CSV file"
    csvobj.parseCsvFile()
    
    if os.path.exists(csvobj.output_file):
        os.remove(csvobj.output_file)
    if os.path.exists(csvobj.output_file.replace(".csv", ".xlsx")):
        os.remove(csvobj.output_file.replace(".csv", ".xlsx"))
    
    if not (args.n or args.p):
        args.n = True
        args.p = True
    
    if args.n:
        if os.path.exists("dbresult"):
            shutil.rmtree("dbresult", ignore_errors=True)
        print "Beginning nmap scans"
        if args.parallel is not None:
            csvobj.runParallelParseDeviceType(args.parallel)
        else:
            csvobj.parseDeviceType()
    if args.p:
        if os.path.exists("notprofiled"):
            shutil.rmtree("notprofiled", ignore_errors=True)
        print "Parsing the nmap scan XML results"
        csvobj.parseMacAddress()
        csvobj.parseXmlResults()
    
    if not args.c:
        convert_csv(csvobj.output_file, csvobj.output_file.replace(".csv", ".xlsx"))
        os.remove(csvobj.output_file)
    
if __name__ == '__main__':
    main(sys.argv[1:])

    
