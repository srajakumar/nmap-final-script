Packages required
=================
1. rancidcmd

Please follow the link for installation related issues,
https://pypi.org/project/rancidcmd/

Please refer the documentation for more info on rancidcmd,
https://media.readthedocs.org/pdf/rancidcmd/latest/rancidcmd.pdf

2. re [Comes along with python]

Files required:
==============

1. nmapper.py = Main python script which executes the code.
2. switch.txt = Please enter the list of switches in this file.

How to run:
==========

Command to execute the script,

python nmapper.py --parallel 5 <argument1> <argument2>

<argument1> - CSV file name along with extension which would be created. 
              Contains MACAddress, IP and Hostname which is then passed as 
              input to get the nmap. Eg: infile.csv
<argument2> - Text file which contains the list of Switch entries. Eg:Switch.txt
